# LesionFinder: Bounding Box Regression for Chest CT
**LesionFinder** is a ResNet-50 (pre-trained) model adapted for the task of object detection and identification of lesions in CT images. Here, we provide our data downloading and preprocessing pipeline, the completed LesionFinder model (In Progress), and the ML code for LesionFinder. The model is trained on the DeepLesion dataset, which may be found in its complete glory [here](https://nihcc.app.box.com/v/DeepLesion/folder/50715173939).

## Installation and Usage
To begin, start by cloning the repository and navigating to the `src/` directory:

```bash via SSH
# Clone the git repository (via SSH)
git clone 'git@gitlab.com:benjamin-ahlbrecht/yolo-lesion-detection.git'

# Navigate to the `src/` directory
cd 'yolo-lesion-detection/src/'
```
```bash via HTTPS
# Clone the git repository (via HTTPS)
git clone 'https://gitlab.com/benjamin-ahlbrecht/yolo-lesion-detection.git'

# Navigate to the 'src/' directory
cd 'yolo-lesion-detection/src/'
```

### Downloading and Preprocessing the DeepLesion Dataset
To download and preprocess the DeepLesion atleast 375 GB of free disk space is recommended. The full dataset can be downloaded into the `Data/Images_png/` directory by calling:

```bash Downloading DeepLesion
python3 data_downloader.py
```

To preprocess the deep lesion dataset, you can execute the `data_processing.ipynb` IPython Notebook. You may use a tool such as `nbconvert` to convert the notebook to a Python script that may be ran directly from the command line. Full preprocessing details may be found in the notebook. Preprocessed tensors will be saved to the `Data/tensors/` directory, which may be compressed using a compression or archiving utility such as `zip` or `tar`.

### Model Training and Evaluation (In Progress)

### Loading and Using LesionFinder (In Progress)
